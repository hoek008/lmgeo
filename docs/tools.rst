Simple GIS operations with Lmgeo tools
======================================

A number of tools can be found in the subdirectory 'toolbox':

* cliplib: for clipping from rasters
* copylib: for copying a raster file to memory and to return it as InMemoryRaster
* punchlib: for carrying out operations with a raster file and vector data, such as zonal statistics
* scalelib: for scaling raster data
* vcliplib: for clipping vector data along the edges of a boundary box, so that the vectors appear well when they are plotted.