Reading and writing raster files with Lmgeo
===========================================

The classes found in the formats subdirectory more or less have the same interface:

*properties:*

* nrows:     the number of rows
* ncols:     the number of columns
* dx:    the length of a pixel side on the ground in the X-direction
* dy:    the length of a pixel side on the ground in the Y-direction
* cellsize:      the width / height of a pixel on the ground; it is usually assumed that dx = dy
* xll:   X-coordinate of the lower left corner of the raster
* yll:   Y-coordinate of the lower left corner of the raster

*methods:*

* constructor, with two arguments indicating the relevant filename and the datatype (e.g. 'i' for integer and 'f' for float)
* open(mode) with mode = 'r' - used to initialise the created instance for reading

  The alternative value for mode is 'w' - used to initialise the created instance for writing:
* open(mode, ncols, nrows, xll, yll, cellsize, nodatavalue) *(single band)*
* open(mode, ncols, nrows, nbands, xll, yll, cellsize, nodatavalue) *(multiband)*

* next() - used to cause the created instance to retrieve the values of the next row
* reset(), to prepare the created instance for reading the file again from the beginning
* writenext(sequence_with_data) - used to cause the created instance to write the values to file for the next line
* flush(), to cause the created instance to really write the previously written data to disk
* close(), to cause the created instance to close the file.

*Example*::

    from formats.asciigrid import AsciiGrid
    import sys
    
    myraster = None
    try:
        fn = 'myraster.asc'
        myraster = AsciiGrid(fn, 'i')
        if not myraster.open('r'):
            raise Exception('Cannot open file %s' % fn)
        else:
            for i in range(myraster.nrows):
                line = myraster.next()
                for k in range(myraster.ncols):
                    if line[k] == 42:
                        print('Found!')
                        sys.exit()
    finally:
        if myraster is not None:
            myraster.close()
                
                     

Please note that Lmgeo supports a number of file formats which make use of separate header files. These often have the extension \*.hdr.
This extension is usually added to the already existing name, so the header file of 'myraster.bil' becomes 'myraster.bil.hdr'. Some
GIS software programmes do not follow this convention and rather expect a header file with name 'myraster.hdr'.

*InMemoryRaster*

The only class that has a slightly different interface is InMemoryRaster. When constructing an instance, the second argument is data. 
It has to be specified either as None or as a sequence with data::

    from formats.inmemoryraster import InMemoryRaster
    
    nrows, ncols = 100, 50
    xll, yll = 0.0, 0.0
    cellsize = 1.0
    nodatavalue = -999
    fn = 'dummy.asc'
    inmemraster = InMemoryRaster(fn, None, 'i')
    if not inmemraster.open('w', ncols, nrows, xll, yll, cellsize, nodatavalue):
        raise Exception("Unable to initialise InMemoryRaster!")






